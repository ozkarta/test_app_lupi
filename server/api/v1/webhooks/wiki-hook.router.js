'use strict'

const WikiHookService = require('./wiki-hook.service');
const router = require('express').Router();

router.post('/handle-page-linking', WikiHookService.handlePageLinking);

module.exports = router;