'use strict'
// Get dependencies
global.__basedir = __dirname;

const express = require('express');
const http = require('http');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const timeout = require('connect-timeout');
const cors = require('cors');

const env = process.env.NODE_ENV || 'development';
const config = require('./config')[env];

class Server {
  constructor() {
    this.allowedOrigins = [
      'http://localhost:4201',
      'http://localhost:4202',
      'http://localhost:4203',
      'http://localhost:4204',
      'http://localhost:4205',
      'http://localhost:5000',
      'http://markethub.ge', 
      '*'
    ];
    this.options = {
      origin: (origin, callback) => {
        if (this.allowedOrigins.indexOf('*')) {
          return callback(null, true);
        }
        if (this.allowedOrigins.indexOf(origin) !== -1) {
          callback(null, true)
        } else {
          callback(new Error('Not allowed by CORS'))
        }
      }
    }
    this.app = express();
    this.server = http.Server(this.app);
    this.v1Routes = require('./api/v1/routes').mainServer();
    this.port = config.PORT;
    this.initApp();
    this.server.listen(this.port, () => console.log(`Our server is running on: ${this.port}`));
  }

  initApp() {
    // Parsers for POST data
    this.app.use(cookieParser());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(cors(this.options));    

    // Set our api routes
    this.app.use('/api/v1', this.v1Routes);

    // Catch all other routes and return 404
    this.app.use('*', (req, res) => {
      return res.status(404).json();
    });

    this.app.set('port', this.port);



    this.app.use(timeout(12000000));
    this.app.use(this.haltOnTimedout);
    this.app.use(this.handleError);
  }

  haltOnTimedout(req, res, next){
    if (!req.timedout) next();
  }

  handleError(error, req, res, next) {
    console.log(error);
    return res.json(error.message);
  }
}

const server = new Server();