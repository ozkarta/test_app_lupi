'use strict'
const express = require('express');
const router = express.Router();

module.exports = {
  mainServer: () => {
    router.use('/web-hooks', require('./webhooks/wiki-hook.router'));
    
    return router;
  }
}