'use strict'
const Promise = require('bluebird');
const encoding = 'utf8';
const https = require('https');
const url = require('url');
const iconv = require('iconv-lite');

const wikiRegexp = /^https:\/\/[a-zA-Z]{2}.wikipedia.org\/wiki\/.*$/g;
const wikiLinkRegexp = /<a\s+(?:[^>]*?\s+)?href=(["'])(.*?)\1/g
const wikiPathRegexp = /<a[^>]* href="([^"]*)"/;
const concurrency = 20;

module.exports = {
  handlePageLinking
}

async function handlePageLinking(req, res, next) {
  let { urlArray, maxClicks } = req.body;
  try {
    return res.status(200).json(await generateResult({urlArray, maxClicks}));
  } catch (error) {
    next(error);
  }
}

// ===========================================================
async function generateResult({urlArray, maxClicks, iteration = 1, fetchedUrls = []}) {
  if (!urlArray || !urlArray.length) return [];
  let urls = [...urlArray];

  // filter before fetching, remove duplicates
  urls = urlArray.filter((url, pos) => {
    return urlArray.indexOf(url) === pos && !!url.match(wikiRegexp);
  });

  // filter before fetching, remove already fetched items
  // prevents infinite loops
  urls = urls.filter((url, pos) => {
    return fetchedUrls.indexOf(url) < 0;
  });

  let data = await fetchUrlsInBatch(urls, concurrency);
  console.log('');
  for (let item of data) {
    item.childUrls = parseAllUrlsFromHtmlString(item.data).map(str => `https://${getUrlOrigin(item.urlString)}${str}`);
    if (item.childUrls && item.childUrls.length && iteration < maxClicks) {
      item.childUrls = await generateResult({urlArray: item.childUrls, maxClicks, iteration: iteration + 1, fetchedUrls});
    }
    fetchedUrls.push(item.urlString);
    urls.push(...item.childUrls)
    delete item.data;
  }

  // final filter, removes duplicates from result
  return urls.filter((url, pos) => {
    return urls.indexOf(url) === pos && !!url.match(wikiRegexp);
  });
}

function parseAllUrlsFromHtmlString(htmlString) {
  let paths = (htmlString.match(wikiLinkRegexp) || []).map(str => (wikiPathRegexp.exec(str) || [])[1]).filter(str => {
    return (str || '').match(/^\/wiki\//) && !str.match(/\/wiki\/File:/);
  });
  return paths;
}

function buildTree(data, urlString, iteration, parsedUrls = []) {
  let result = { urlString, parents: [], iteration };

  for (let item of data) {
    if (urlString === item.urlString) continue;
    if (item.data.indexOf(`${getUrlPath(urlString)}`) >= 0 || item.data.indexOf(`${urlString}`) >= 0) {
      if (parsedUrls.indexOf(item.urlString) >= 0) {
        continue;
      }
      parsedUrls.push(urlString);
      let parentTree = buildTree(data, item.urlString, iteration + 1, parsedUrls);
      result.parents.push(parentTree);
    }
  }

  if (!result.parents.length) {
    result.depth = 0;
  } else {
    result.depth = Math.min(...result.parents.map(item => item.depth || 0)) + 1;
  }
  return result;
}

function getUrlPath(urlString) {
  return url.parse(urlString).path;
}

function getUrlOrigin(urlString) {
  return url.parse(urlString).host;
}

function fetchRequestedHttpsUrl(urlString, callback) {
  let urlOptions = url.parse(urlString);
  let bufferArray = [];
  let req = https.request(urlOptions, (response) => {
    response.on('data', (data) => {
      bufferArray.push(data);
    });
    response.on('end', (data) => {
      let responseBody = iconv.decode(Buffer.concat(bufferArray), encoding);
      return callback(null, responseBody);
    });
  });

  req.on('error', (err) => {
    console.log('ERROR FETCHING URL', urlString);
    return callback(err, null);
  });

  req.end();
}

async function fetchRequestedHttpsUrlPromise(urlString) {
  return new Promise((resolve, reject) => {
    return fetchRequestedHttpsUrl(urlString, (err, data) => {
      if (err) {
        return reject(err);
      }
      return resolve(data);
    })
  });
}

function fetchUrlsInBatch(urls, concurrency) {
  let counter = 1;
  return Promise.map(urls, (urlString) => {
    return new Promise((resolve, reject) => {
      fetchRequestedHttpsUrlPromise(urlString)
        .then(data => {
          process.stdout.clearLine();
          process.stdout.cursorTo(0);
          process.stdout.write("Fetched " + counter++);
          return resolve({ data, urlString });
        }).catch(exception => resolve({ exception: exception }));
    })
  }, { concurrency });
}